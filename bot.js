const Discord = require('discord.js');
const auth = require('./auth.json');
const bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

const prefix = '!';
const subString = "live";
const restler = require("restler");
let streamer;
let jsonString;

bot.on('ready', () => {
    console.log('Discord bot online');
});

bot.on('message', function(message){

    // command to check if twitch streamer is live, if so send some data to user in discord
    if(message.content.startsWith(prefix + 'stream')) {
        streamer = message.content.slice(7).trim().split(' ');

        restler.get("https://api.twitch.tv/helix/streams?user_login=" + streamer, {
            headers: {
                "Client-ID": auth.clientId
            }
        }).on("success", function(streamerData) {
            jsonString = JSON.stringify(streamerData);
            
        if(jsonString.includes(subString)) {
            let lowerName = streamerData.data[0].user_name.toLowerCase();
            message.channel.send({
            "embed": {
                "title": streamerData.data[0].user_name,
                "description": streamerData.data[0].title + "\n" + "Started at " + streamerData.data[0].started_at,
                "url": "https://twitch.tv/"+lowerName,
                "color": 6570404,
                "image": {
                    "url": "https://static-cdn.jtvnw.net/previews-ttv/live_user_"+lowerName+"-1280x720.jpg"
                },
                "footer": {
                    "text": streamerData.data[0].viewer_count + " " + "Viewers",
                    "icon_url": "https://i.imgur.com/UEtydXZ.png"
                }
            }
            });
        } else {
            message.channel.send("Stream is offline or does not exist!");
        }
        });
    }
});
bot.login(auth.token);
